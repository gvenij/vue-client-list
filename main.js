Vue.component('client', {
	template: `<table name="fade" is="transition-group" v-bind:class="{'opened': isToggled, 'closed': !isToggled}">
	<tr :key="client.name + 1" class="client-name">
		<th colspan="5" class="client-name">
			{{client.name}}
			<button @click="toggleClient()" class="toggle-button">
				<span>&rsaquo;</span>
			</button>
		</th>

	</tr>
	<tr class="bold-heading" :key="client.name + 2">
		<td></td>
		<td>Budget</td>
		<td>Cost</td>
		<td>Conversions</td>
		<td>CPA</td>
	</tr>
	<tr v-if="client.facebookads" :key="client.name + 3">
		<td class="bold-heading">Facebookads</td>
		<td>{{ client.facebookads.budget }}</td>
		<td>{{ client.facebookads.cost }} ({{ Math.round(this.$parent.$options.methods.safeDivision(client.facebookads.budget,client.facebookads.cost)) }}%)</td>
		<td>{{ client.facebookads.conversions }}</td>
		<td>{{ this.$parent.$options.methods.safeDivision(client.facebookads.cost,client.facebookads.conversions) }}</td>
	</tr>
	<tr v-if="client.bingads" :key="client.name + 4">
		<td class="bold-heading">Bingads</td>
		<td>{{ client.bingads.budget }}</td>
		<td>{{ client.bingads.cost }} ({{ Math.round(this.$parent.$options.methods.safeDivision(client.bingads.budget,client.bingads.cost)) }}%)</td>
		<td>{{ client.bingads.conversions }}</td>
		<td>{{ this.$parent.$options.methods.safeDivision(client.bingads.cost,client.bingads.conversions) }}</td>
	</tr>
	<tr v-if="client.adwords" :key="client.name + 5">
		<td class="bold-heading">Adwords</td>
		<td>{{ client.adwords.budget }}</td>
		<td>{{ client.adwords.cost }} ({{ Math.round(this.$parent.$options.methods.safeDivision(client.adwords.budget,client.adwords.cost)) }}%)</td>
		<td>{{ client.adwords.conversions }}</td>
		<td>{{ this.$parent.$options.methods.safeDivision(client.adwords.cost,client.adwords.conversions) }}</td>
	</tr>
	<tr :key="client.name + 6">
		<td class="bold-heading">Aggregated</td>
		<td>{{ client.aggregated.budget }}</td>
		<td>{{ client.aggregated.cost }} ({{ Math.round(this.$parent.$options.methods.safeDivision(client.aggregated.budget,client.aggregated.cost)) }}%)</td>
		<td>{{ client.aggregated.conversions }}</td>
		<td>{{ this.$parent.$options.methods.safeDivision(client.aggregated.cost,client.aggregated.conversions) }}</td>
	</tr>
</table>`,
	name: 'client',
	props: ['client', 'toggleState'],

	data: function() {
		return {
			isToggled: this.toggleState
		};
	},

	methods: {
		toggleClient: function() {
			this.isToggled = !this.isToggled;
		}
	},

	watch: {
		toggleState: function() {
			this.isToggled = this.toggleState;
		}
	}
});

var app = new Vue({
	el: '#app',
	data: {
		clients: [],
		filterText: '',
		sources: ['bingads', 'adwords', 'facebookads'],
		isToggledAll: false
	},
	created() {
		this.getClients();
	},
	methods: {
		getClients: function() {
			axios
				.get('https://frontendtask.appspot.com/api/clients')
				.then(response => {
					this.clients = response.data;
				})
				.catch(e => {
					console.log(e);
				});
		},
		safeDivision: function(a, b) {
			var res = a / b;
			if (isNaN(res) || !isFinite(res)) {
				return 0;
			} else {
				return res;
			}
		},
		updateSourceLIst: function(src, event) {
			var updated;
			if (event.target.classList.contains('disabled')) {
				event.target.classList.remove('disabled');
				updated = this.sources;
				updated.push(src);
				this.sources = updated;
			} else {
				if (this.sources.length > 1) {
					event.target.classList.add('disabled');
					updated = this.sources.filter(function(item) {
						return item != src;
					});
					this.sources = updated;
				}
			}
		},
		sourceUpdate: function(src, event) {
			this.updateSourceLIst(src, event);
			axios
				.get('https://frontendtask.appspot.com/api/clients', {
					params: {
						source: this.sources.join()
					}
				})
				.then(response => {
					this.clients = response.data;
					console.log(this.clients);
				})
				.catch(e => {
					console.log(e);
				});
		},
		toggleAll: function() {
			this.isToggledAll = !this.isToggledAll;
		}
	},
	computed: {
		filteredClients: function() {
			var that = this;
			return this.clients.filter(function(item) {
				return (
					item.name.toLowerCase().indexOf(that.filterText.toLowerCase()) != -1
				);
			});
		}
	}
});
